function mergeSort(array){
  if(array.length<2)return;
    var subOne=new Array(parseInt(array.length/2));
    var subTwo=new Array(array.length-parseInt(array.length/2));
    var i=0;
    for(;i<subOne.length;i++)subOne[i]=array[i];
    for(var j=0;j<subTwo.length;j++)subTwo[j]=array[i++];
    mergeSort(subOne);
    mergeSort(subTwo);
    merge(subOne,subTwo,array);
}
function merge(subArrayOne,subArrayTwo,mainArray){
    var i=0,j=0,l1=subArrayOne.length,l2=subArrayTwo.length;
    while(i+j<mainArray.length){
        if(j===l2||(i<l1 && subArrayOne[i]<subArrayTwo[j]))mainArray[i+j]=subArrayOne[i++];
        else mainArray[i+j]=subArrayTwo[j++];
    }
}
function sortInput()
{
    var givenInput=document.getElementById('array').value.trim();
    
    var arrayMerge=givenInput.split(' ');
 document.getElementById('givenInput').innerHTML='<h1>GIVEN INPUT : '+JSON.stringify(arrayMerge)+'</h1>';
    console.log(arrayMerge);
    mergeSort(arrayMerge);
    console.log(arrayMerge);
    document.getElementById('mergeSortResult').innerHTML='<h1>RESULT USING MERGE SORT : '+JSON.stringify(arrayMerge)+'</h1>';
    var arrayQuick=givenInput.split(' ');
    
    quickSort(arrayQuick,0,arrayQuick.length-1);
     document.getElementById('quickSortResult').innerHTML='<h1>RESULT USING QUICK SORT : '+JSON.stringify(arrayQuick)+'</h1>';
    console.log(arrayQuick);
}
function swap(array,a,b){
    var temp=array[a];
    array[a]=array[b];
    array[b]=temp;
}
function partition(array,start,end){
    var partitionIndex=start;
    var pivot=array[end];
    for(var i=start;i<end;i++)if(array[i]<=pivot)swap(array,i,partitionIndex++);
    swap(array,partitionIndex,end);
    return partitionIndex;
}
function quickSort(array,start,end){
    if(start>=end)return;
    var partitionIndex=partition(array,start,end);
    quickSort(array,start,partitionIndex-1);
    quickSort(array,partitionIndex+1,end);
}