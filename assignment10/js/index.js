var formTable=document.getElementById('formTable');
var arrayInput=document.getElementById('arrayInput');
var nElem=document.getElementById('n');
var printButton=document.getElementById('printLastN');

printButton.onclick=function(event){
    var array=arrayInput.value.trim();
    var inputArray=array.split(' ');
    //console.log(inputArray);
    var size=inputArray.length;
    var n=nElem.value;
    if(n>=size)printArray(array,inputArray,0,size);
    else{
     printArray(array,inputArray,size-n,size);
     //console.log('start index '+(size-n)+'end index '+ size);
    }
}
function printArray(original,array,start,end){
    var sb=new StringBuilder('<td colspan="2" >LAST ');
    sb.append(end-start);
    sb.append(' Elements are  : ');
    for(var i=start;i<end;i++)sb.append(array[i]+" ");
    sb.append('</td>');
    //console.log(sb);
    var rows=formTable.rows;
    if(rows.length===3){
    var newRow=formTable.insertRow(-1);
    newRow.innerHTML=sb.toString();
    }else rows[rows.length-1].innerHTML=sb.toString();
}

function StringBuilder(value)
{
    this.strings = new Array("");
    this.append(value);
}

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function (value)
{
    if (value)
    {
        this.strings.push(value);
    }
}

StringBuilder.prototype.clear = function ()
{
    this.strings.length = 1;
}


StringBuilder.prototype.toString = function ()
{
    return this.strings.join("");
}